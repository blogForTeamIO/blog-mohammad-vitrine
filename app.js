const path = require('path');
const express = require('express');
const expressEdge = require('express-edge');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const expressSession = require('express-session');
const connectMongo = require('connect-mongo');
const edge = require('edge.js');
const nconf = require('nconf');

const port = process.env.PORT || 3000;

const app = new express();

const router = require('./route');

/** 
	Connecting to the MongoDB database, creating node-blog if necessary
**/

nconf.argv().env().file('keys.json');

const user = nconf.get('mongoUser');
const pass = nconf.get('mongoPass');
const host = nconf.get('mongoHost');

var uri = `mongodb://${user}:${pass}@${host}`;
if (nconf.get('mongoDatabase')) {
  uri = `${uri}/${nconf.get('mongoDatabase')}`;
}

mongoose.connect(uri, { useNewUrlParser: true })
	.then(() => 'You are now connected to Mongo!')
	.catch(err => console.error('Something went wrong', err));

const mongoStore = connectMongo(expressSession);

app.use(fileUpload());

app.use(express.static('public'));
app.use(expressEdge);
app.set('views', __dirname + '/views');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(expressSession({
	secret: 'secret',
	resave: false,
  	saveUninitialized: false,
	store: new mongoStore({
		mongooseConnection: mongoose.connection
	})
}));

app.use('*', (req, res, next) => {
	edge.global('auth', req.session.user)
	next();
});

app.use('/', router);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

app.listen(port, function (err) {
    console.log('[%s] Listening on http://localhost:%d', app.settings.env, 3000);
});
