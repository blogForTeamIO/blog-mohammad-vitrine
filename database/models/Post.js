const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
	title: String,
	description: String,
	content: String,
	username: String,
	userId: String,
	image: String,
	createdAt: {
		type: Date,
		default: new Date()
	}
});

PostSchema.index({
	'title': 'text',
	'description': 'text',
	'content': 'text',
	'username': 'text'
});

const Post = mongoose.model('Post', PostSchema);

module.exports = Post;