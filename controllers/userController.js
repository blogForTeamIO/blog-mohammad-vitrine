const bcrypt = require('bcryptjs');
const User = require('../database/models/User');

module.exports.login = (req, res) => {
	res.render('login');
}

module.exports.logout = (req, res) => {
	req.session.destroy();
	res.redirect('/');		
}

module.exports.loginUser = async (req, res) => {
	const {
		username,
		password
	} = req.body;

	await User.findOne({
		username
	}, (error, user) => {
		if(user) {
			bcrypt.compare(password, user.password, (error, same) => {
				if (same) {
					req.session.user = user;
					res.redirect('/')
				} else {
					res.redirect('/auth/login')
				}
			})
		} else {
			return res.redirect('/auth/login')
		}
	})
}

module.exports.createUser = (req, res) => {
	res.render('register');
}

module.exports.storeUser = (req, res) => {
	User.create(req.body, (error, user) => {
		console.log(error);
		if(error) {
			return res.redirect('/auth/register')
		}

		res.redirect('/');
	});
}

module.exports.getUserPage = (req, res) => {
	res.render('user');
}