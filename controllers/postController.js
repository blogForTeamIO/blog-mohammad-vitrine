const path = require('path');
const Post = require('../database/models/Post');

module.exports.deletePost = async (req, res) => {
	Post.deleteOne({ 
		_id: req.body.id
	}, (error, post) => {
		res.redirect('/');
	});
}

module.exports.goToModifyPost = (req, res) => {
	var post = req.session.selectedPost;

	res.render('modifyPost', {
		post
	});
}

module.exports.goToPostCreation = (req, res) => {
	res.render('create');
}

module.exports.modifyPost = async (req, res) => {
	let {
		image
	} = req.files;

	if (image) {
		await image.mv(path.resolve(__dirname, '..', 'public/posts', image.name));
		image = `/posts/${image.name}`;
	} else {
		image = req.session.selectedPost.image;
	}

	await Post.updateOne({ 
		_id: req.session.selectedPost._id 
	}, { 
		$set: {
			...req.body,
			'image': image 
		}
	}, () => {
		res.redirect('/post/' + req.session.selectedPost._id);
	}).catch(
	(err) => {
		console.log(err);
	});	
}

module.exports.storePosts = async (req, res) => {
	const {
		image
	} = req.files;

	if (image) {
		image.mv(path.resolve(__dirname, '..', 'public/posts', image.name));
	}

	Post.create({
		username: req.session.user.username,
		userId: req.session.user._id,
		...req.body,
		image: image ? `/posts/${image.name}` : `/posts/default.jpg`
	}, (error, post) => {
		res.redirect('/');
	});
}

module.exports.getPostById = (req, res) => {
	const post = req.session.selectedPost;

	res.render('post', {
		post
	});
}

module.exports.searchPosts = async (req, res) => {
	var posts = await Post.find({
		$text : { 
			$search : req.query.searchWords
		}
	}).catch(
	(err) => {
		console.log(err);
	});

	res.render('index', {
		posts: posts || []
	});
}

module.exports.getAllPosts = async (req, res) => {
	const posts = await Post.find({}).catch(
		(err) => {
			console.log(err);
		});

	res.render('index', {
		posts
	});
}