FROM node:latest

LABEL author="Moha"

ENV NODE_ENV=production 
ENV PORT=3000

RUN mkdir -p /data
WORKDIR /data

COPY package.json /data

RUN npm install

ADD . /data

EXPOSE $PORT

ENTRYPOINT ["npm", "start"]
