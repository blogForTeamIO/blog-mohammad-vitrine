//Route system

const express = require('express');
const router = express.Router();

const postController = require('./controllers/postController');
const userController = require('./controllers/userController');

const storePost = require('./middleware/storePost');
const storeUser = require('./middleware/storeUser');
const auth = require('./middleware/auth');
const getSelectedPost = require('./middleware/selectedPost');
const redirectIfAuthenticated = require('./middleware/redirectIfAuthenticated')

router.get('/', postController.getAllPosts)
.use('/posts/store', storePost)
.get('/post/:id', getSelectedPost, postController.getPostById)
.get('/posts/new', auth, postController.goToPostCreation)
.post('/posts/store', postController.storePosts)
.post('/post/modify', postController.goToModifyPost)
.post('/post/modifyPost', postController.modifyPost)
.post('/post/delete', postController.deletePost)
.get('/search', postController.searchPosts)
.get('/auth/register', redirectIfAuthenticated,userController.createUser)
.post('/users/register', redirectIfAuthenticated, storeUser, userController.storeUser)
.get('/auth/login', redirectIfAuthenticated, userController.login)
.post('/users/login', redirectIfAuthenticated, userController.loginUser)
.get('/auth/logout', userController.logout)
.get('/user', userController.getUserPage)

.get('/about', function(req, res) {
	res.render('about');
})

.get('/contact', function (req, res) {
    res.render('contact');
})

module.exports = router;