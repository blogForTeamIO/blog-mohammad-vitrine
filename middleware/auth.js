const User = require('../database/models/User')
 
module.exports = (req, res, next) => {
    User.findById(req.session.user ? req.session.user._id : null, (error, user) => {
        if (error || !user) {
            return res.redirect('/');
        }
 
        next()
    })
}