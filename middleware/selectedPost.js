const Post = require('../database/models/Post');

module.exports = async (req, res, next) => {
	const post = await Post.findById(req.params.id).catch(
		(err) => {
			console.log(err);
			res.redirect('/');
		});

	req.session.selectedPost = post;

	next()
}