const Post = require('../database/models/Post');

module.exports = async (req, res, next) => {
	Post.findById(req.params.id, (error, post) => {
		req.session.selectedPost = post;
	});

	next();
}